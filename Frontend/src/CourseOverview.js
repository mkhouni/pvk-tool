const m = require('mithril');

module.exports = {
  view() {
    return m('div', 'You are an admin. Nice!');
  },
};
